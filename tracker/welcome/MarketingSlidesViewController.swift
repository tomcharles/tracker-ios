//
//  MarketingSlidesViewController.swift
//  tracker
//
//  Created by Thomas Charles on 11/2/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import UIKit
import SnapKit

class MarketingSlidesViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidLayoutSubviews() {
        view.addBottomBorderWithColor(color: TrackerColors.grayDarker, weight: 1.0)
    }
    
    func setupViews() {
        view.backgroundColor = TrackerColors.gray
    }
}
