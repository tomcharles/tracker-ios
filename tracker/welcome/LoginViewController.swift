//
//  LoginViewController.swift
//  tracker
//
//  Created by Thomas Charles on 11/3/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import UIKit
import SnapKit
import Result

class LoginViewController: UIViewController {
    
    private var adjustedBottomConstraint: Constraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupGestures()
        emailField.becomeFirstResponder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupGestures() {
        loginButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onLoginButtonClicked)))
    }
    
    @objc func onLoginButtonClicked(_ gestureRecognizer: UITapGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            self.startLoadingState()
            _ = AuthenticationService(apiClient: ApiClient())
                .login(email: emailField.text ?? "", password: passwordField.text ?? "", onSuccess: onLoginSuccess, onError: onLoginFailure)
        }
    }
    
    private func onLoginSuccess(me: Me) {
        showError(message: "[apiKey]: \(me.apiToken)")
    }
    
    private func onLoginFailure(error: DomainError) {
        switch error {
        case .invalidEmailAddress:
            self.showError(message: "Invalid email address")
        case .invalidPassword:
            self.showError(message: "Invalid password")
        case .serviceError:
            self.showError(message: "There was an error connecting to the server")
        }
    }
    
    private func startLoadingState() {
        errorMessage.text = ""
    }
    
    private func showError(message: String) {
        errorMessage.text = message
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        if let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect {
            self.adjustedBottomConstraint?.deactivate()
            self.forgotPasswordButton.snp.makeConstraints { make in
                let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
                let newBottom = isKeyboardShowing ? keyboardFrame.height + 16 : 40
                
                self.adjustedBottomConstraint = make.bottom.equalTo(-newBottom).constraint
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        setupViewBorders()
    }
    
    func setupViews() {
        view.backgroundColor = TrackerColors.gray
        view.addSubview(headerView)
        view.addSubview(emailField)
        view.addSubview(passwordField)
        view.addSubview(errorMessage)
        view.addSubview(loginButton)
        view.addSubview(forgotPasswordButton)
        
        headerView.snp.makeConstraints { make in
            make.leading.equalTo(view).offset(16)
            make.trailing.equalTo(view).offset(-16)
            make.top.equalTo(view).offset(48)
            make.height.equalTo(80)
        }
        
        emailField.snp.makeConstraints { make in
            make.leading.trailing.equalTo(view)
            make.top.equalTo(headerView.snp.bottom)
            make.height.equalTo(48)
        }
        
        passwordField.snp.makeConstraints { make in
            make.leading.trailing.height.equalTo(emailField)
            make.top.equalTo(emailField.snp.bottom)
        }
        
        errorMessage.snp.makeConstraints { make in
            make.leading.trailing.equalTo(headerView)
            make.top.equalTo(passwordField.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        loginButton.snp.makeConstraints { make in
            make.leading.trailing.equalTo(headerView)
            make.height.equalTo(48)
            make.bottom.equalTo(forgotPasswordButton.snp.top).offset(-8)
        }
        
        forgotPasswordButton.snp.makeConstraints { make in
            make.leading.trailing.height.equalTo(loginButton)
            self.adjustedBottomConstraint = make.bottom.equalTo(view).offset(-40).constraint
        }
    }
    
    func setupViewBorders() {
        emailField.addTopBorderWithColor(color: TrackerColors.grayDarker, weight: 1)
        emailField.addBottomBorderWithColor(color: TrackerColors.grayDarker, weight: 1)
        passwordField.addBottomBorderWithColor(color: TrackerColors.grayDarker, weight: 1)
    }
    
    let headerView: UILabel = {
        let label = UILabel()
        label.text = "Welcome back!"
        label.font = TrackerFonts.heading
        return label
    }()
    
    let emailField: UITextField = {
        let field = UITextField()
        field.placeholder = "Email"
        field.backgroundColor = .white
        field.setLeftPaddingPoints(16)
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        return field
    }()
    
    let passwordField: UITextField = {
        let field = UITextField()
        field.placeholder = "Password"
        field.backgroundColor = .white
        field.setLeftPaddingPoints(16)
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.isSecureTextEntry = true
        return field
    }()
    
    let errorMessage: UILabel = {
        let label = UILabel()
        label.textColor = TrackerColors.error
        return label
    }()
    
    let loginButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle("Log In", for: .normal)
        return button
    }()
    
    let forgotPasswordButton: UIButton = {
        let button = OpenButton()
        button.setTitle("Forgot Password?", for: .normal)
        return button
    }()
}
