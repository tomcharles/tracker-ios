//
//  ViewController.swift
//  tracker
//
//  Created by Thomas Charles on 11/2/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import UIKit
import SnapKit

class WelcomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        addChildren()
        setupViews()
        setupGestures()
    }
    
    func addChildren() {
        addChild(sliderViewController)
        sliderViewController.didMove(toParent: self)
    }
    
    func setupViews() {
        view.backgroundColor = .white
        view.addSubview(sliderViewController.view)
        view.addSubview(loginButton)
        view.addSubview(signupButton)
        
        sliderViewController.view.snp.makeConstraints { make -> Void in
            make.top.leading.trailing.equalTo(view)
            make.height.equalTo(view.frame.height * (3/4))
        }
        
        loginButton.snp.makeConstraints { make -> Void in
            make.top.equalTo(sliderViewController.view.snp.bottom).offset(32)
            make.leading.equalTo(view).offset(32)
            make.trailing.equalTo(view).offset(-32)
            make.height.equalTo(56)
        }
        
        signupButton.snp.makeConstraints { make -> Void in
            make.top.equalTo(loginButton.snp.bottom).offset(16)
            make.leading.trailing.height.equalTo(loginButton)
        }
    }
    
    func setupGestures() {
        loginButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onLoginButtonClicked)))
    }
    
    @objc func onLoginButtonClicked(_ gestureRecognizer: UITapGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            let loginViewController = LoginViewController()
            present(loginViewController, animated: true)
        }
    }
    
    let sliderViewController: MarketingSlidesViewController = {
        return MarketingSlidesViewController()
    }()
    
    let loginButton: UIButton = {
        let button = PrimaryButton()
        button.setTitle("Log In", for: .normal)
        return button
    }()
    
    let signupButton: UIButton = {
        let button = OpenButton()
        button.setTitle("Sign up via browser", for: .normal)
        return button
    }()
}
