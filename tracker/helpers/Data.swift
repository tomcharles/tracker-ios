//
//  Data.swift
//  tracker
//
//  Created by Thomas Charles on 11/3/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import Foundation

extension Data {
    func asString() -> NSString? {
        return NSString(data: self, encoding: String.Encoding.utf8.rawValue)
    }
}
