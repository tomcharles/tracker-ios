//
//  UIView.swift
//  tracker
//
//  Created by Thomas Charles on 11/2/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import UIKit

extension UIView {
    func addTopBorderWithColor(color: UIColor, weight: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: weight)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, weight: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - weight, width: self.frame.size.width, height: weight)
        self.layer.addSublayer(border)
    }
}
