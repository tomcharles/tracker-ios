//
//  TrackerFonts.swift
//  tracker
//
//  Created by Thomas Charles on 11/4/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import UIKit

class TrackerFonts {
    
    static let heading: UIFont = UIFont.systemFont(ofSize: 40)
    
}
