//
//  TrackerColors.swift
//  tracker
//
//  Created by Thomas Charles on 11/2/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import UIKit

class TrackerColors {
    
    static let gray: UIColor = UIColor.from(hex: "#F5F6F8")
    static let grayDarker: UIColor = UIColor.from(hex: "#D5DAE0")
    
    static let primary: UIColor = UIColor.from(hex: "#007AFF")
    static let primaryLighter: UIColor = UIColor.from(hex: "#9FD1F7")
    
    static let error: UIColor = UIColor.from(hex: "#E3615C")
    
}
