//
//  Buttons.swift
//  tracker
//
//  Created by Thomas Charles on 11/2/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import UIKit

class OpenButton: UIButton {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setTitleColor(TrackerColors.primary, for: .normal)
        setTitleColor(TrackerColors.primaryLighter, for: .highlighted)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PrimaryButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setBackgroundColor(color: TrackerColors.primary, forState: .normal)
        setBackgroundColor(color: TrackerColors.primaryLighter, forState: .highlighted)
        layer.cornerRadius = 8
        clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
