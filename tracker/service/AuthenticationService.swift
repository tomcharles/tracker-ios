//
//  AuthenticationService.swift
//  tracker
//
//  Created by Thomas Charles on 11/3/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import Result

class AuthenticationService {
    
    let apiClient: ApiClient
    
    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }
    
    func login(email: String, password: String, onSuccess: @escaping (Me) -> Void, onError: @escaping (DomainError) -> Void) {
        let _ = validatedCredentials(email: email, password: password)
            .map { credentials -> (ValidatedEmailAddress, ValidatedPassword) in
                sendLoginRequest(credentials: credentials, onSuccess: onSuccess, onError: onError)
                return credentials
            }.mapError { err -> DomainError in
                onError(err)
                return err
        }
    }
    
    private func validatedCredentials(email: String, password: String) -> Result<(ValidatedEmailAddress, ValidatedPassword), DomainError> {
        return ValidatedEmailAddress.make(from: email)
            .flatMap(withValidPassword(password: password))
    }
    
    private func withValidPassword(password: String) -> (_ emailAddress: ValidatedEmailAddress) -> Result<(ValidatedEmailAddress, ValidatedPassword), DomainError> {
        return { validatedEmailAddress in
            ValidatedPassword.make(from: password)
                .map { (validatedEmailAddress, $0) }
        }
    }
    
    private func sendLoginRequest(
        credentials: (ValidatedEmailAddress, ValidatedPassword),
        onSuccess: @escaping (Me) -> Void,
        onError: @escaping (DomainError) -> Void) {
        self.apiClient.authenticate(AuthenticationRequest(emailAddress: credentials.0, password: credentials.1)) { done in
            switch done {
            case .success(let response):
                onSuccess(response)
            case .error:
                onError(DomainError.serviceError)
            }
        }
    }
}
