//
//  ApiClient.swift
//  tracker
//
//  Created by Thomas Charles on 11/3/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import enum Result.Result
import Alamofire

enum ApiError: Error {
    case unknownError
}

protocol ApiRequest {
    associatedtype Response: Codable
    var resourceName: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
}

enum ApiResponse<T> {
    case success(val: T)
    case error(err: ApiError)
}

class ApiClient {
    private let baseEndpointUrl = URL(string: "https://www.pivotaltracker.com/services/v5/")
    
    func authenticate(_ request: AuthenticationRequest, onDone: @escaping (ApiResponse<Me>) -> Void) {
        Alamofire.request(endpoint(for: request))
            .authenticate(user: request.emailAddress, password: request.password)
            .responseData(completionHandler: onResponse(onDone: onDone))
    }
    
    func send<T: ApiRequest>(_ request: T, onDone: @escaping (ApiResponse<T.Response>) -> Void) {
        Alamofire.request(endpoint(for: request), method: request.method, parameters: request.parameters)
            .responseData(completionHandler: onResponse(onDone: onDone))
    }
    
    private func endpoint<T: ApiRequest>(for request: T) -> URL {
        guard let url = URL(string: request.resourceName, relativeTo: baseEndpointUrl) else {
            fatalError("Bad resourceName: \(request.resourceName)")
        }
        
        return url
    }

    fileprivate func onResponse<T: Decodable>(onDone: @escaping (ApiResponse<T>) -> Void) -> (DataResponse<Data>) -> Void {
        return { response in
            if response.result.isSuccess,
                let data = response.result.value,
                let result = try? JSONDecoder().decode(T.self, from: data) {
                onDone(ApiResponse.success(val: result))
            } else {
                onDone(ApiResponse.error(err: ApiError.unknownError))
            }
        }
    }
}
