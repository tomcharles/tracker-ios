//
//  AuthenticationRequest.swift
//  tracker
//
//  Created by Thomas Charles on 11/3/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import Foundation
import Alamofire

struct Me: Codable {
    let apiToken: String
    let emailAddress: String
    let initials: String
    let fullName: String
    let username: String
    
    enum CodingKeys: String, CodingKey {
        case apiToken = "api_token"
        case emailAddress = "email"
        case initials
        case fullName = "name"
        case username
    }
}

struct AuthenticationRequest: ApiRequest {
    typealias Response = Me
    var resourceName: String = "me"
    var method: HTTPMethod = HTTPMethod.get
    var parameters: Parameters?
    
    let emailAddress: String
    let password: String
    
    init(emailAddress: ValidatedEmailAddress, password: ValidatedPassword) {
        self.emailAddress = emailAddress.emailAddress
        self.password = password.password
    }
}
