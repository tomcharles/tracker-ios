//
//  Password.swift
//  tracker
//
//  Created by Thomas Charles on 11/4/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import Foundation
import Result

class ValidatedPassword: Encodable {
    
    let password: String
    
    private init(from password: String) {
        self.password = password
    }
    
    static func make(from password: String) -> Result<ValidatedPassword, DomainError> {
        if (password.count == 0) {
            return .failure(.invalidPassword)
        }
        return .success(ValidatedPassword(from: password))        
    }
}
