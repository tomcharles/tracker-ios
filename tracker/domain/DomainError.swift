//
//  DomainError.swift
//  tracker
//
//  Created by Thomas Charles on 11/4/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import Foundation
import Result

enum DomainError: Error {
    case invalidEmailAddress
    case invalidPassword
    case serviceError
}
