//
//  EmailAddress.swift
//  tracker
//
//  Created by Thomas Charles on 11/3/18.
//  Copyright © 2018 Tom Charles. All rights reserved.
//

import Foundation
import Result

class ValidatedEmailAddress: Encodable {
    
    let emailAddress: String
    
    private init(from emailAddress: String) {
        self.emailAddress = emailAddress
    }
    
    static func make(from emailAddress: String) -> Result<ValidatedEmailAddress, DomainError> {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if (emailTest.evaluate(with: emailAddress)) {
            return .success(ValidatedEmailAddress(from: emailAddress))
        }
        
        return Result.failure(.invalidEmailAddress)
    }
}
